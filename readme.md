Example app for McKinsey code challenge.
=======================================
*Developed by Maxim Rogoza.*

This app requires [node.js](https://nodejs.org/).

First, go to the project's folder and install required npm packages running following command:

        npm install
        

Then execute next command to run the app:

        npm start

Then open a browser and goto http://localhost:3000/

Application renders data from the *\data\reports.json* file. **Block** and **Resolve** buttons send a command 
to the server and hide the current line after receiving a response. The commands received by server are displayed in the server log.


PS. When I did this job I did not use any database as it was not required in the description of the challenge.

I tried to perform the task as much as possible close to the description. However, in real life I would use 
some kind of reactive framework such as Meteor, and for the same time I spent for the example application I could make 
a similar functionality with the database, registration, 
the possibility of multi-user work and many other things.