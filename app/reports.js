module.exports = (() => {
	const fs = require('fs')

	result = {};
	result.BLOCK = 'block';
	result.RESOLVE = 'resolve';
	
	// Returns list of reports
	result.getReports = function(callback) {
		fs.readFile('data/reports.json', (err, data) => {
			if (err)
				callback(err);
			else {
				let json = data.toString()
					.split(/(?={")/)
					.map(x => JSON.parse(x));
				callback(null, json);	
			}
		})
	};
	
	// Mark report as resolved or blocked
	result.updateReport = function(reportId, command, body, callback) {
		console.log({
			reportId: reportId,
			command: command,
			requestBody: body
		});
		callback(!reportId 
		    || (command !== result.BLOCK && command !== result.RESOLVE) 
			|| !body 
			|| !body.ticketState 
			|| body.ticketState !== 'CLOSED' 
			? 'Invalid operation' : null);
	}
	
	return result;
})();
