module.exports = function(app) { 
	const path = require('path')
	const express = require('express')
	const reports = require('./reports')

	app.use(express.static(path.join(__dirname, 'public')));
	app.use("/public", express.static(path.join(__dirname, 'public')));

	app.get('/', (request, response) => {
		reports.getReports((err, data) => {
			if (err) {
				console.log('Error: ', err);
				response.status(500).send('Error 500. Internal Server error');
			}
			else 
				response.render('reports', { reports: data	});
		})
	})

	app.all('/reports/:reportId', (request, response, next) => {
		function processReport(request, command, response) {
			reports.updateReport(request.params.reportId, command, request.body, (err) => {
				if (err) {
					console.log('Error: ', err);
					response.status(500).send('Error 500. Internal Server error');
				}
				else 
					response.status(200).send('ok');
			});
		}	
		if (request.method === 'PUT')
			processReport(request, reports.RESOLVE, response)
		else if (request.method === 'REPORT')
			processReport(request, reports.BLOCK, response)
		else
			next();
	})

	app.all('*', (request, response) => {
		response.status(404).send('Error 404. Page not found')
	})
}
